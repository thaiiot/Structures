# Communication Subsystem

# 1. Introduction.

CubeSats are nano and pico size satellites that are standardized in cubic sizes. The main goal of CubeSats has been to make hard-earned space science and technology readily accessible for students, engineers and even for some governments at a fraction of design/build/launch costs in comparison to standard satellite projects. 
The most distinct properties of CubeSats are weight and size. A standard cube shaped satellites must be less than 1 kg and its size must be approximately 10 cm per size that is called one unit -1U- CubeSat. Correspondingly 2U CubeSats have 20*10*10 cm size and 3U’s have 30*10*10 cm size, and also their weights increase respectively. This standardization makes easy compatibility between developers. Towards this end, in this paper we present the design and analysis of a modular 3U structure that gives provides the much needed flexibility to the satellite designers during the system integration phase. This modular structure is first tailored towards THAIIOT, and the project aims to demonstrate on-orbit an to prevent floods and risk forecast

** Table 1: Variations on the CubeSat standard regarding size and mass
Structural design shall aim for simple load paths, a maximization in the use of conventional materials, simplified interfaces, and easy integration. Due to the size of the satellite and small expense budget, this was done with the philosophy of maximizing usable interior space, while minimizing the complexity and cost of the design. Due to the weight constraints, the structure must be the lightest possible to allow more margins for the other subsystems.
The primary structure is the backbone, or major load path, between the spacecraft’s components and the launch vehicle. Secondary structures include support beams, booms, trusses, and solar panels. We refer to the smallest structures, such as boxes that house electronics and brackets that support electrical cables, as tertiary.

| CubeSat Designation | Size | Mass |
| ------ | ------ | ------ |
| Cube (1U) | 10 cm x 10 cm x 10 cm | 1 kg |
| Double Cube (2U) | 10 cm x 10 cm x 20 cm | 2 kg |
| Triple Cube (3U) | 10 cm x 10 cm x 30 cm | 3 kg |
| 6 Pack (6U) | 10 cm x 20 cm x 30 cm | 6 kg |

# 2. Subsystem Requirement.

The design of the THAIIOT structure design refers to the ICD provided by the Soyuz2 to any cube maker that will launch its satellites using Soyuz2 launch vehicle. The launcher provided various requirements regarding the CubeSat structure design specifications to be launched. The requirements consist of Mass Requirement, Dimension Requirement, and Material Structure Requirement 
The CubeSat launch concept selected permits the launch of multiple secondary payloads which may be stacked into a launcher tube, called P-POD (Poly-Picosatellite Orbital Deployer), developed by P-POD is a standard deployment system that ensures all CubeSat developers conform to common physical requirements. The P-POD plays a critical role as the interface between the launch vehicle and CubeSats. It utilizes a tubular design and can hold up to 34 cm x 10 cm x 10 cm of deployable hardware space. The following figure 1of the P-POD and the following figure 2of the dimension 3U CubeSat.
